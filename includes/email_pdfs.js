Drupal.behaviors.emailpdfs = function(context) {
  $('.email-pdf-link:not(.email-pdf-processed)').addClass('email-pdf-processed').each(function() {
    $(this).attr('href', $(this).attr('href') + '&email-pdf-link=1');
  });
}
