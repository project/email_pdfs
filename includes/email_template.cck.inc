<?php

/**
 * @file
 * Contains pdf template.
 */

/**
 * Store the string version of the content type to import.
 */
function email_pdfs_cck_email_pdf_template() {
  return '$content["type"]  = array (
    "name" => "Email PDF",
    "type" => "email_pdf_template",
    "description" => "An email template with a PDF attachment that users "
      . "can send by email.",
    "title_label" => "Template name",
    "body_label" => "Email prefix",
    "min_word_count" => "0",
    "help" => "Regardless of what the email message contains, this text is "
      . "appended to the message. Leave blank to use the default text. "
      . "Enter <none> to omit the header from the email.",
    "node_options" =>
    array (
      "status" => true,
      "promote" => false,
      "sticky" => false,
      "revision" => false,
    ),
    "subscriptions_workflow" =>
    array (
      "n_new" => "n_new",
      "n_unpub" => "n_unpub",
      "n_pub" => "n_pub",
      "c_new" => "c_new",
      "c_unpub" => "c_unpub",
      "c_pub" => "c_pub",
    ),
    "language_content_type" => "0",
    "upload" => "1",
    "forward_display" => 1,
    "search_block" => false,
    "old_type" => "email_pdf_template",
    "orig_type" => "",
    "module" => "node",
    "custom" => "1",
    "modified" => "1",
    "locked" => "0",
    "image_attach" => "0",
    "image_attach_maximum" => "0",
    "image_attach_size_teaser" => "thumbnail",
    "image_attach_size_body" => "thumbnail",
    "nodewords_edit_metatags" => 0,
    "nodewords_metatags_generation_method" => "0",
    "nodewords_metatags_generation_source" => "2",
    "nodewords_use_alt_attribute" => 1,
    "nodewords_filter_modules_output" =>
    array (
      "imagebrowser" => false,
      "img_assist" => false,
    ),
    "nodewords_filter_regexp" => "",
    "comment" => "0",
    "comment_default_mode" => "4",
    "comment_default_order" => "1",
    "comment_default_per_page" => "50",
    "comment_controls" => "3",
    "comment_anonymous" => 0,
    "comment_subject_field" => "1",
    "comment_preview" => "1",
    "comment_form_location" => "0",
    "abuse_content_node_type_email_pdf_template" => 0,
    "fivestar" => 0,
    "fivestar_stars" => 5,
    "fivestar_labels_enable" => 1,
    "fivestar_label_0" => "Cancel rating",
    "fivestar_label_1" => "Poor",
    "fivestar_label_2" => "Okay",
    "fivestar_label_3" => "Good",
    "fivestar_label_4" => "Great",
    "fivestar_label_5" => "Awesome",
    "fivestar_label_6" => "Give it @star/@count",
    "fivestar_label_7" => "Give it @star/@count",
    "fivestar_label_8" => "Give it @star/@count",
    "fivestar_label_9" => "Give it @star/@count",
    "fivestar_label_10" => "Give it @star/@count",
    "fivestar_style" => "average",
    "fivestar_text" => "dual",
    "fivestar_title" => 1,
    "fivestar_feedback" => 1,
    "fivestar_unvote" => 0,
    "fivestar_position_teaser" => "hidden",
    "fivestar_position" => "below",
    "print_display" => 0,
    "print_display_comment" => 0,
    "print_display_urllist" => 0,
    "print_mail_display" => 0,
    "print_mail_display_comment" => 0,
    "print_mail_display_urllist" => 0,
    "print_pdf_display" => 0,
    "print_pdf_display_comment" => 0,
    "print_pdf_display_urllist" => 0,
    "i18n_newnode_current" => 0,
    "i18n_required_node" => 0,
    "i18n_lock_node" => 0,
    "i18n_node" => 1,
    "i18nsync_nodeapi" =>
    array (
      "name" => false,
      "status" => false,
      "promote" => false,
      "moderate" => false,
      "sticky" => false,
      "revision" => false,
      "parent" => false,
      "taxonomy" => false,
      "comment" => false,
      "files" => false,
      "field_email_pdf_subject" => false,
      "field_email_pdf_file" => false,
      "field_email_pdf_message" => false,
      "field_email_pdf_add_link" => false,
    ),
    "better_formats_allowed" =>
    array (
      1 => false,
      2 => false,
      3 => false,
      4 => false,
      5 => false,
    ),
    "better_formats_defaults" =>
    array (
      "node-1" =>
      array (
        "format" => "1",
        "weight" => "0",
      ),
      "node-2" =>
      array (
        "format" => "1",
        "weight" => "0",
      ),
      "node-3" =>
      array (
        "format" => "2",
        "weight" => "0",
      ),
      "node-4" =>
      array (
        "format" => "2",
        "weight" => "0",
      ),
      "comment-1" =>
      array (
        "format" => "1",
        "weight" => "0",
      ),
      "comment-2" =>
      array (
        "format" => "1",
        "weight" => "0",
      ),
      "comment-3" =>
      array (
        "format" => "2",
        "weight" => "0",
      ),
      "comment-4" =>
      array (
        "format" => "2",
        "weight" => "0",
      ),
    ),
  );
  $content["groups"]  = array (
    0 =>
    array (
      "label" => "Links",
      "group_type" => "standard",
      "settings" =>
      array (
        "form" =>
        array (
          "style" => "fieldset",
          "description" => "",
        ),
        "display" =>
        array (
          "description" => "",
          5 =>
          array (
            "format" => "fieldset",
            "exclude" => 0,
          ),
          "teaser" =>
          array (
            "format" => "fieldset",
            "exclude" => 0,
          ),
          "full" =>
          array (
            "format" => "fieldset",
            "exclude" => 0,
          ),
          4 =>
          array (
            "format" => "fieldset",
            "exclude" => 0,
          ),
          2 =>
          array (
            "format" => "fieldset",
            "exclude" => 0,
          ),
          3 =>
          array (
            "format" => "fieldset",
            "exclude" => 0,
          ),
          "cck_blocks" =>
          array (
            "format" => "fieldset",
            "exclude" => 0,
          ),
          "token" =>
          array (
            "format" => "fieldset",
            "exclude" => 0,
          ),
          "label" => "above",
        ),
      ),
      "weight" => "0",
      "group_name" => "group_pdf_links",
    ),
  );
  $content["fields"]  = array (
    0 =>
    array (
      "label" => "Default subject",
      "field_name" => "field_email_pdf_subject",
      "type" => "text",
      "widget_type" => "text_textfield",
      "change" => "Change basic information",
      "weight" => "-4",
      "rows" => 5,
      "size" => "60",
      "description" => "The default subject on the email form. This can "
        . "be overridden by the user before sending the email.",
      "default_value" =>
      array (
      ),
      "default_value_php" => "",
      "default_value_widget" => NULL,
      "group" => false,
      "required" => 0,
      "multiple" => "0",
      "text_processing" => "0",
      "max_length" => "512",
      "allowed_values" => "",
      "allowed_values_php" => "",
      "global_cck_blocks_settings" => "1",
      "op" => "Save field settings",
      "module" => "text",
      "widget_module" => "text",
      "columns" =>
      array (
        "value" =>
        array (
          "type" => "text",
          "size" => "big",
          "not null" => false,
          "sortable" => true,
          "views" => true,
        ),
      ),
      "display_settings" =>
      array (
        "label" =>
        array (
          "format" => "above",
          "exclude" => 0,
        ),
        5 =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        "teaser" =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        "full" =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        4 =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        2 =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        3 =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        "cck_blocks" =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        "token" =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
      ),
    ),
    1 =>
    array (
      "label" => "PDF Attachment",
      "field_name" => "field_email_pdf_file",
      "type" => "filefield",
      "widget_type" => "filefield_widget",
      "change" => "Change basic information",
      "weight" => "-3",
      "file_extensions" => "pdf",
      "progress_indicator" => "bar",
      "file_path" => "email_pdfs",
      "max_filesize_per_file" => "",
      "max_filesize_per_node" => "",
      "description" => "",
      "group" => false,
      "required" => 1,
      "multiple" => "0",
      "list_field" => "1",
      "list_default" => 1,
      "description_field" => "1",
      "global_cck_blocks_settings" => "1",
      "op" => "Save field settings",
      "module" => "filefield",
      "widget_module" => "filefield",
      "columns" =>
      array (
        "fid" =>
        array (
          "type" => "int",
          "not null" => false,
          "views" => true,
        ),
        "list" =>
        array (
          "type" => "int",
          "size" => "tiny",
          "not null" => false,
          "views" => true,
        ),
        "data" =>
        array (
          "type" => "text",
          "serialize" => true,
          "views" => true,
        ),
      ),
      "display_settings" =>
      array (
        "label" =>
        array (
          "format" => "above",
          "exclude" => 0,
        ),
        5 =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        "teaser" =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        "full" =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        4 =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        2 =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        3 =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        "cck_blocks" =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        "token" =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
      ),
    ),
    2 =>
    array (
      "label" => "Default message",
      "field_name" => "field_email_pdf_message",
      "type" => "text",
      "widget_type" => "text_textarea",
      "change" => "Change basic information",
      "weight" => "-1",
      "rows" => "5",
      "size" => 60,
      "description" => "",
      "default_value" =>
      array (
      ),
      "default_value_php" => "",
      "default_value_widget" =>
      array (
        "field_email_pdf_message" =>
        array (
          0 =>
          array (
            "value" => "",
            "_error_element" =>
              "default_value_widget][field_email_pdf_message][0][value",
          ),
        ),
      ),
      "group" => false,
      "required" => 0,
      "multiple" => "0",
      "text_processing" => "0",
      "max_length" => "",
      "allowed_values" => "",
      "allowed_values_php" => "",
      "global_cck_blocks_settings" => "1",
      "op" => "Save field settings",
      "module" => "text",
      "widget_module" => "text",
      "columns" =>
      array (
        "value" =>
        array (
          "type" => "text",
          "size" => "big",
          "not null" => false,
          "sortable" => true,
          "views" => true,
        ),
      ),
      "display_settings" =>
      array (
        "label" =>
        array (
          "format" => "above",
          "exclude" => 0,
        ),
        5 =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        "teaser" =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        "full" =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        4 =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        2 =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        3 =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        "cck_blocks" =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        "token" =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
      ),
    ),
    3 =>
    array (
      "label" => "Add link to",
      "field_name" => "field_email_pdf_add_link",
      "type" => "nodereference",
      "widget_type" => "nodereference_autocomplete",
      "change" => "Change basic information",
      "weight" => "1",
      "autocomplete_match" => "contains",
      "size" => "60",
      "description" => "",
      "default_value" =>
      array (
      ),
      "default_value_php" => "",
      "default_value_widget" =>
      array (
        "field_email_pdf_add_link" =>
        array (
          0 =>
          array (
            "nid" =>
            array (
              "nid" => "",
              "_error_element" =>
                "default_value_widget][field_email_pdf_add_link][0][nid][nid",
            ),
            "_error_element" =>
              "default_value_widget][field_email_pdf_add_link][0][nid][nid",
          ),
        ),
      ),
      "group" => "group_pdf_links",
      "required" => 0,
      "multiple" => "1",
      "referenceable_types" =>
      array (
        "caregiver_page" => "caregiver_page",
        "caregiver_story" => "caregiver_story",
        "course_didactic_page" => "course_didactic_page",
        "coursepage" => "coursepage",
        "about" => "about",
        "page" => "page",
        "program" => "program",
        "caregiver_tip" => 0,
        "clinical_assessment" => 0,
        "course_credit_type" => 0,
        "course_icon" => 0,
        "site_blog" => 0,
        "did_you_know" => 0,
        "email_pdf_template" => 0,
        "website" => 0,
        "faq" => 0,
        "forum" => 0,
        "image" => 0,
        "internal_resource" => 0,
        "blog" => 0,
        "multichoice" => 0,
        "poll" => 0,
        "product" => 0,
        "program_credit_type" => 0,
        "quiz" => 0,
        "references" => 0,
        "resource_list_page" => 0,
        "training_programs" => 0,
        "flashnode" => 0,
        "webform" => 0,
        "slideshow" => 0,
      ),
      "advanced_view" => "--",
      "advanced_view_args" => "",
      "global_cck_blocks_settings" => "1",
      "op" => "Save field settings",
      "module" => "nodereference",
      "widget_module" => "nodereference",
      "columns" =>
      array (
        "nid" =>
        array (
          "type" => "int",
          "unsigned" => true,
          "not null" => false,
          "index" => true,
        ),
      ),
      "display_settings" =>
      array (
        "label" =>
        array (
          "format" => "above",
          "exclude" => 0,
        ),
        5 =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        "teaser" =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        "full" =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        4 =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        2 =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        3 =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        "cck_blocks" =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
        "token" =>
        array (
          "format" => "default",
          "exclude" => 0,
        ),
      ),
    ),
  );
  $content["extra"]  = array (
    "title" => "-5",
    "body_field" => "-2",
    "revision_information" => "7",
    "author" => "6",
    "options" => "8",
    "comment_settings" => "12",
    "menu" => "2",
    "book" => "5",
    "path" => "13",
    "attachments" => "11",
    "detailed_question" => "4",
    "print" => "10",
    "xmlsitemap" => "9",
    "nodewords" => "3",
  );';
}
