<?php

/**
 * @file
 * Page callback functions.
 */

/**
 * Page callback to send an email.
 */
function email_pdfs_send_template($template = NULL, $ajax = NULL) {
  if (!$template && variable_get('email_pdfs_default_template', 0)) {
    $template = email_pdfs_template_load(variable_get('email_pdfs_default_template', 0));
  }
  // Class email-pdf-link is added to the end of the url by javascript.
  $ajax = is_null($ajax) ? !empty($_GET['email-pdf-link']) && module_exists('lightbox2') : $ajax;
  $output = drupal_get_form('email_pdfs_send_template_form', $template, $ajax);

  if ($ajax) {
    print $output;
    print '<script type="text/javascript">Drupal.attachBehaviors();</script>';
    exit;
  }
  return $output;
}

/**
 * Main form callback to send an email with pdf.
 */
function email_pdfs_send_template_form(&$form_state, $template = NULL, $ajax = FALSE) {
  global $user;
  $form = array();
  $form['from-email'] = array(
    '#type' => 'textfield',
    '#title' => t('Your email address'),
    '#description' => t(''),
    '#default_value' => !empty($user->mail) ? $user->mail : NULL,
    '#size' => 100,
    '#required' => TRUE,
  );
  $form['from-name'] = array(
    '#type' => 'textfield',
    '#title' => t('Your name'),
    '#size' => 100,
    '#required' => TRUE,
  );
  $form['recipients'] = array(
    '#type' => 'textfield',
    '#title' => t('Recipients'),
    '#description' => t('Enter one or more email addresses, separated by commas.'),
    '#size' => 100,
    '#required' => TRUE,
  );
  $form['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => $template && !empty($template->field_email_pdf_subject) ? $template->field_email_pdf_subject[0]['value'] : NULL,
    '#size' => 100,
    '#required' => TRUE,
  );
  if ($header = email_pdfs_template_header_text($template)) {
    $form['header'] = array(
      '#type' => 'item',
      '#title' => t('Message'),
      '#value' => $header,
      '#disabled' => TRUE,
    );
  }
  $form['body'] = array(
    '#type' => 'textarea',
    '#default_value' => $template && !empty($template->field_email_pdf_message) ? $template->field_email_pdf_message[0]['value'] : NULL,
    '#required' => TRUE,
    '#cols' => 100,
    '#description' => t('You may add a customized message in the box above to be included with your email.'),
  );
  if (!$header) {
    $form['body']['#title'] = t('Message');
  }

  $attachments = array();
  if (!empty($template) && !empty($template->field_email_pdf_file)) {
    foreach ($template->field_email_pdf_file as $pdf) {
      $attachments[] = l(t('Preview PDF'), file_create_url($pdf['filepath']), array('attributes' => array('target' => '_blank')));
    }
  }
  if (!empty($attachments)) {
    $form['attachments'] = array(
      '#type' => 'item',
      '#value' => implode('<br />', $attachments),
    );
  }

  $form['copy-me'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send a copy to myself'),
  );
  $form['send-email'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
    '#ajax' => array(
      'submitter' => TRUE,
    ),
  );
  if ($ajax) {
    $form['#ajax'] = array(
      'enabled' => TRUE,
    );
  }
  // If this form is in a lightbox then it will be centered by default. Left
  // align all of the elements.
  $form['#attributes'] = array(
    'style' => 'text-align: left',
  );
  $form['#email_pdf_template'] = $template;
  return $form;
}

/**
 * Validate the send form.
 */
function email_pdfs_send_template_form_validate(&$form, &$form_state) {
  $from = trim($form_state['values']['from-email']);
  if (!valid_email_address($from)) {
    form_set_error('from-email', t('Invalid email address.'));
  }

  $recipients = explode(',', trim($form_state['values']['recipients']));
  foreach ($recipients as $id => $email) {
    $email = trim($email);
    if (!valid_email_address($email)) {
      form_set_error('recipients', t('%email is an invalid email address.', array('%email' => $email)));
    }
  }
}

/**
 * Form submit handler. Send an email pdf.
 */
function email_pdfs_send_template_form_submit(&$form, &$form_state) {
  $sender = array(
    'name' => trim($form_state['values']['from-name']),
    'mail' => trim($form_state['values']['from-email']),
  );

  // No HTML from end user allowed.
  $body = drupal_html_to_text(trim($form_state['values']['body']));
  // Add the header text.
  $header = email_pdfs_template_header_text($form['#email_pdf_template']);
  $body = $header . $body;

  // Get the attachments.
  $attachments = array();
  if (!empty($form['#email_pdf_template']) && !empty($form['#email_pdf_template']->field_email_pdf_file)) {
    $attachments = $form['#email_pdf_template']->field_email_pdf_file;
  }

  // Strip out duplicate recipient email address.
  $recipient = array();
  $recipients = explode(',', trim($form_state['values']['recipients']));
  foreach ($recipients as $id => $email) {
    $email = trim($email);
    $recipient[$email] = $email;
  }
  if (!empty($form_state['values']['copy-me'])) {
    $recipient[$sender['mail']] = $sender['mail'];
  }
  mimemail($sender, implode(',', $recipient), trim($form_state['values']['subject']), $body, NULL, array(), NULL, $attachments, 'email_pdfs');
  drupal_set_message(t('Your message has been successfully sent.'));
}

/**
 * Get the header text for a template.
 */
function email_pdfs_template_header_text($template) {
  if (!empty($template->body)) {
    if ($template->body == '<none>') {
      return '';
    }
    return check_markup($template->body, $template->format, FALSE);
  }
  if (($text = variable_get('email_pdfs_email_header', array('header' => '', 'format' => 0))) && $text['header']) {
    return check_markup($text['header'], $text['format'], FALSE);
  }
}

/**
 * Menu callback for settings in the module.
 */
function email_pdfs_admin_form(&$form_state) {
  $form = array();
  $form['email_pdfs_default_template'] = array(
    '#type' => 'select',
    '#title' => t('Default template'),
    '#options' => array(0 => '<none>') + email_pdfs_load_by_type(EMAIL_PDFS_NODE_TYPE),
    '#description' => t('The default template to use when none is specified.'),
    '#default_value' => variable_get('email_pdfs_default_template', 0),
  );

  $text = variable_get('email_pdfs_email_header', array('header' => '', 'format' => 0));
  $form['email_pdfs_email_header'] = array(
    '#tree' => TRUE,
  );
  $form['email_pdfs_email_header']['header'] = array(
    '#type' => 'textarea',
    '#title' => t('Default email header'),
    '#default_value' => $text['header'],
  );
  $form['email_pdfs_email_header']['format'] = filter_form($text['format']);
  return system_settings_form($form);
}

/**
 * Load all nodes of a certain type.
 */
function email_pdfs_load_by_type($type) {
  $nodes = array();
  $query = db_rewrite_sql("SELECT nid, title FROM {node} n WHERE type = '%s'", 'n');
  $results = db_query($query, $type);
  while ($item = db_fetch_object($results)) {
    $nodes[$item->nid] = $item->title;
  }
  return $nodes;
}
