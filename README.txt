This Drupal 6 module creates links to send emails with PDFs attached.
'Email this page as PDF' links can be added to multiple pages via a single
template. This module has two levels of access. Administrators of the
site can set up a templates which let users send a pre-defined PDF copy of
a page. For users of the site, they are able to send a personalized message
along with the PDF copy, as well as send a copy to themselves.

This module allows administrators to have more control over the layout and
design of the PDF copy that's emailed, or attach a completely separate PDF.
It also allows administrators to add an Email prefix message to be included
with the email, which cannot be changed or removed by the user.

Module Dependencies:

Ajax
Content (CCK)
Content Import
FileField
Lightbox 2
Mime Mail
Node Reference
Option Widgets
Text

Overview of Features:
The content creation by the administrators have the following fields which can
be filled in:
Template Name: A name given to the template to reference it.
Default Subject: The suggested subject of the email being sent. It can be
 changed by the user.
PDF Attachment: The uploaded PDF of the page you want to reference, formatted
 correctly (or any other PDF you choose to upload).
Email Prefix: A message that is included with the user's message in the email
 sent. This cannot be changed by the user.
Default Message: A message that can be used as a "suggested" message. The
 user can change this to be more personalized if they wish.
 Add Link To: The page the Email PDF link will appear on. This is a node
 reference field that will pull from any enabled content types.


An "Email this page as PDF" link is then added to the referenced page(s).
When the user clicks on this link, they get a pop-up and the following fields
can be filled in by them:

Your Email Address: The email address of the user.
Your Name: The user's name.
Recipients: The email address(es) of the people the PDF should be sent to.
Subject: The suggested message is included, but can be changed by the user.
Message: The administrative message, if included, is shown immediately before
 the text box, which includes a suggested message. The user can change the
 text box message to be more personalized.
Send A Copy to Myself: The user can check the box to have the message sent
 to them as well.
There is also a Preview PDF link, which opens the included PDF in a separate
tab, so that the user can review before sending.
